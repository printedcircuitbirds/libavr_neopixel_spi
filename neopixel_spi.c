/*
 * neopixel_spi.c
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#include <avr/io.h>
#include <stdint.h>
#include "neopixel_spi.h"

/*
 * Local defines
 */


/*
 * Private functions
 */
static void neopixel_spi_send_byte(uint8_t byte)
{
	for (uint8_t i = 0; i <= 7; i++)
	{
		while(!(LED_SPI.INTFLAGS & SPI_DREIF_bm)) {
			// Wait for empty buffer
		}
		
		// Write from MSB to LSB
		if (byte & 0x80) {
			LED_SPI.DATA = NEOPIXEL_ONE;
		} else {
			LED_SPI.DATA = NEOPIXEL_ZERO;
		}
		byte <<= 1;
	}
}

/*
 * Public functions
 */
void neopixel_spi_init(void)
{
	LED_SPI.CTRLB = SPI_BUFEN_bm | SPI_SSD_bm | SPI_MODE_0_gc;
	#if ((F_CPU == 24000000ul) || (F_CPU == 20000000ul) || (F_CPU == 16000000ul))
		LED_SPI.CTRLA = SPI_MASTER_bm | SPI_PRESC_DIV4_gc | SPI_ENABLE_bm;
	#elif ((F_CPU == 12000000ul) || (F_CPU == 10000000ul) || (F_CPU == 8000000ul))
		LED_SPI.CTRLA = SPI_MASTER_bm | SPI_PRESC_DIV4_gc | SPI_ENABLE_bm | SPI_CLK2X_bm;
	#else
		#error "F_CPU needs to be one of the above settings?"
	#endif

	// Update PORTMUX to alternate SPI pinout, if used
	#if defined(PORTMUX_CTRLB) // Used by tiny 0- and 1-series
		PORTMUX.CTRLB |= LED_SPI_PORT_ALT;
	#elif defined(PORTMUX_TWISPIROUTEA) // Used by mega 0-series
		PORTMUX.TWISPIROUTEA |= LED_SPI_PORT_ALT;
	#elif defined(PORTMUX_SPIROUTEA) // Used by tiny 2-series and AVR Dx++
		PORTMUX.SPIROUTEA |= LED_SPI_PORT_ALT;
	#endif

	// Set MOSI to inverted output
	LED_SPI_PORT.OUTSET = LED_DATA_PIN;
	LED_SPI_PORT.LED_DATA_PINCTRL = PORT_INVEN_bm;
	LED_SPI_PORT.DIRSET = LED_DATA_PIN;
}

void neopixel_spi_configure_single(color_t color)
{
	for(uint8_t idx = 0; idx < NEOPIXEL_LED_BYTES; idx++) {
		neopixel_spi_send_byte(color.array[idx]);
	}
}

void neopixel_spi_configure_array(color_t *array, uint16_t length)
{
	uint8_t *ptr = (uint8_t *) array;
	for(uint16_t idx = 0; idx < length * NEOPIXEL_LED_BYTES; idx++) {
		neopixel_spi_send_byte(ptr[idx]);
	}
}
