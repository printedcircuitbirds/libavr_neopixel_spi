# AVR Neopixel SPI library

avr-gcc library for controlling neopixel leds using the SPI peripheral in:

* tinyAVR 0-series
* tinyAVR 1-series
* tinyAVR 2-series
* megaAVR 0-series
* AVR Dx and Ex devices, and probably future devices as well

It runs the SPI ~8x faster than the required neopixel frequency to send one byte of SPI data corresponding to one bit of neopixel data.

## How to use

1. Add this repo as a git submodule or download to your project folder
2. Copy [config/neopixel_config.h](config/neopixel_config.h) to your project and configure for your pinout and type of LEDs
3. Add the four ```neopixel``` source files to the project.
    Microchip Studio does not have an "add existing folder" selection, so the workaround is to click Show All Files and then include the files: ![Explorer Setup 1](example/studio_explorer_include.png "Solution Explorer - Show All Files - Include in Project")
4. Add the location of the config file and the location of the library to the compiler include directories.
    In Microchip Studio it is done like this: ![Toolchain Setup](example/studio_dirs.png "Toolchain - AVR/GNU C Compiler - Directories")
5. Include the library in your project files like this:

    ```c
    #include <neopixel.h>
    ```

See [neopixel_example.c](example/neopixel_example.c) for an example of how to use the driver.

### Interrupts

Because neopixels are relatively picky on the timing, this driver is sending data in polling mode to avoid the overhead of entering and exiting interrupts all the time.

If interrupts are handled during a neopixel update they may mess up the timing.
However, because of the buffer mode in the SPI there is a small window for a short interrupt routine without messing up the timing.

If larger interrupt routines are needed, it may be useful to run the configure function inside an [ATOMIC_BLOCK](https://www.nongnu.org/avr-libc/user-manual/group__util__atomic.html).
