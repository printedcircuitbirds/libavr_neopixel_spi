/*
 * neopixel.c
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#include <avr/io.h>
#include <stdint.h>
#include "neopixel.h"

/*
 * Local defines
 */


/*
 * Private functions
 */


/*
 * Public functions
 */

void neopixel_configure_constant(color_t color, uint16_t length)
{
	while(length--) {
		neopixel_configure_single(color);
	}
}

void neopixel_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t length)
{
	neopixel_configure_constant(constant, single_position);
	neopixel_configure_single(single);
	neopixel_configure_constant(constant, length-single_position-1);
}

void neopixel_configure_off(uint16_t length)
{
	neopixel_configure_constant((color_t) {.channel = 0}, length);
}

void neopixel_configure_off_and_single(color_t color, uint16_t position, uint16_t length)
{
	neopixel_configure_constant_and_single(color, position, (color_t) {.channel = 0}, length);
}

void neopixel_configure_2d_array(color_t* array, uint8_t rows, uint8_t columns)
{
	uint8_t i, j;
	for(i = 0; i < columns; i++) {
		if(i % 2) { //Every odd row flips direction
			for(j = 0; j < rows; j++) {
				neopixel_spi_configure_single(array[(i + 1)*rows - 1 - j]);
			}
		}
		else {
			for(j = 0; j < rows; j++) {
				neopixel_spi_configure_single(array[i * rows + j]);
			}
		}
	}

}


/*
 * Converts HSB (Hue, Saturation, Brightness) values
 * to their corresponding 8-bit RGB values and returns
 * the results as a color_t
 *
 * Legal ranges:
 * h - 0..1535
 * s - 0..255
 * b - 0..255
 *
 * NOTE:
 * Optimized for speed
 * No rounding
 * b is saturated at 254 to avoid
 */
color_t hsb2rgb(hsb_t config)
{
	uint16_t h = config.h;
	uint8_t s = config.s;
	uint8_t b = config.b;

	uint8_t c = (b * s) / 256;
	uint8_t m = b >= c ? b - c : 0;
	uint8_t x = (h % 256) * c / 256;
	uint8_t sector = h >> 8;

	if (sector == 0) return (color_t) {.r=c+m   , .g=x+m   , .b=0+m   };
	if (sector == 1) return (color_t) {.r=c-x+m , .g=c+m   , .b=0+m   };
	if (sector == 2) return (color_t) {.r=0+m   , .g=c+m   , .b=x+m   };
	if (sector == 3) return (color_t) {.r=0+m   , .g=c-x+m , .b=c+m   };
	if (sector == 4) return (color_t) {.r=x+m   , .g=0+m   , .b=c+m   };
	if (sector == 5) return (color_t) {.r=c+m   , .g=0+m   , .b=c-x+m };
	return (color_t) {0};
}

color_t rotate_pixel_hue(uint8_t value, uint8_t brightness)
{
	static uint16_t pong_hue = 0;
	pong_hue += value;
	if (pong_hue >= HSB_MAX_HUE)
	{
		pong_hue -= HSB_MAX_HUE;
	}
	return hsb2rgb((hsb_t) {.h=pong_hue, .s=0xff, .b=brightness});
}