/*
 * neopixel.h
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#ifndef NEOPIXEL_H_
#define NEOPIXEL_H_

/*
 * Includes
 */
#include "neopixel_spi.h"

/*
 * Redefines of functions to allow easy switch between drivers
 */
#define neopixel_init							neopixel_spi_init
#define neopixel_configure_single				neopixel_spi_configure_single
#define neopixel_configure_array				neopixel_spi_configure_array
#define neopixel_configure_constant				neopixel_spi_configure_constant
#define neopixel_configure_constant_and_single	neopixel_spi_configure_constant_and_single

/*
 * Defines for the hue-saturation-brightness functions
 */
#define HSB_MAX_HUE			1536

/*
 * Data type for the hue functions
 */
typedef struct {
	uint16_t h;
	uint8_t s;
	uint8_t b;
} hsb_t;

/*
 * Public functions
 */

void neopixel_configure_constant(color_t color, uint16_t length);
void neopixel_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t length);
void neopixel_configure_off(uint16_t length);
void neopixel_configure_off_and_single(color_t color, uint16_t position, uint16_t length);
void neopixel_configure_2d_array(color_t* array, uint8_t rows, uint8_t columns);

color_t hsb2rgb(hsb_t config);
color_t rotate_pixel_hue(uint8_t value, uint8_t brightness);

#endif /* NEOPIXEL_H_ */
