/*
 * neopixel_spi.h
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#ifndef NEOPIXEL_SPI_H_
#define NEOPIXEL_SPI_H_

/*
 * Include the project config
 */
#include <neopixel_config.h>

/* 
 * Timing defines for the transmission of one SPI byte per Neopixel bit
 *
 * Source: https://wp.josh.com/2014/05/13/ws2812-neopixels-are-not-so-finicky-once-you-get-to-know-them/
 * Zero high voltage time: Min 200ns, Max 500ns
 * One high voltage time: Min 550ns
 * Zero/One low voltage time: Min 450ns, Max 5000ns
 *
 * For F_CPU = 20 or 10 MHz => 625 kbit/s = ~26k RGB/s or ~20k RGBW/s
 *   zero bit = 25% duty SPI byte = 8bit/(5MHz) * 25% = 400ns high, 1200ns low
 *   one bit  = 50% duty SPI byte = 8bit/(5MHz) * 50% = 800ns high, 800ns low
 * For F_CPU = 16 or 8 MHz => 500 kbit/s = ~21k RGB/s or ~16k RGBW/s
 *   zero bit = 25% duty SPI byte = 8bit/(4MHz) * 25% = 500ns high, 1500ns low (very close to the limit!)
 *   one bit  = 50% duty SPI byte = 8bit/(4MHz) * 50% = 1000ns high, 1000ns low
 */
#define NEOPIXEL_ZERO		((uint8_t) ~0xc0)
#define NEOPIXEL_ONE		((uint8_t) ~0xf0)

/*
 * The data type used for configuring the LEDs
 */
#ifdef NEOPIXEL_TYPE_RGBW
#define NEOPIXEL_LED_BYTES   (4)

typedef union {
	struct {
		uint8_t g;
		uint8_t r;
		uint8_t b;
		uint8_t w;
	};
	uint32_t channel;
	uint8_t array[4];
} color_t;

#else
#define NEOPIXEL_LED_BYTES   (3)

typedef __uint24 uint24_t;

typedef union {
	struct {
		uint8_t g;
		uint8_t r;
		uint8_t b;
	};

#ifdef NEOPIXEL_TYPE_WWA
	struct {
		uint8_t g;
		uint8_t r;
		uint8_t b;
	};
#endif

	uint24_t channel;
	uint8_t array[3];
} color_t;

#endif // NEOPIXEL_TYPE_RGBW

/*
 * Private functions
 */

// static void neopixel_spi_send_byte(uint8_t byte);

/*
 * Public functions
 */

void neopixel_spi_init(void);
void neopixel_spi_configure_single(color_t color);
void neopixel_spi_configure_array(color_t *array, uint16_t length);

#endif /* NEOPIXEL_SPI_H_ */
